#ifndef SRC_GUI_SCRIPT_MENU_H_
#define SRC_GUI_SCRIPT_MENU_H_

int initialize_script_menu(gboolean UpdateScriptPath);
int refresh_scripts(gchar **error);
void fill_script_paths_list();
void siril_get_on_script_pages();

#endif /* SRC_GUI_SCRIPT_MENU_H_ */
